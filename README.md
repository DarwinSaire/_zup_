

# Case

#### Description

A empresa "Zap" quer desenvolver um produto de notícias no mundo. Para isso vamos criar uma automação para ler tweets e gerar artigos. Analise o dataset abaixo, e responda as perguntas.

**Dataset:** https://www.kaggle.com/arkhoshghalb/twitter-sentiment-analysis-hatred-speech



#### Q1

Gostaríamos de evitar publicações racistas. Utilizando o dataset do Twitter, conseguimos identificar esses comentários? Existem tópicos ou assuntos que teriam uma probabilidade maior de gerar esse tipo de comentário?

Visualização do banco de dados

<div align="center"><img src="imgs/dataset_view.png" alt="non-trivial image" width="40%" align="center"><br></div> <br>



Porcentagem de tweets racistas / sexistas e tweets sem conteúdo ofensivo.

<div align="center"><img src="imgs/proportion_pos_neg.png" alt="non-trivial image" width="50%" align="center"><br></div> <br>

Análise da distribuição dos conjuntos de treinamento e teste (tamanho dos tweets)

<div align="center"><img src="imgs/length_tweets_trainvstest.png" alt="non-trivial image" width="50%" align="center"><br></div> <br>

Análise da distribuição do conjunto de treinamento (racista / sexista ou não)

<table>
<tbody>
  <tr>
      <td>
        <div align="center"><img src="imgs/length_tweets_negvspos.png" alt="non-trivial image" width="100%" align="center"><br>por tamanho dos tweets</div> <br>
      </td>
      <td>
        <div align="center"><img src="imgs/length_words_negvspos.png" alt="non-trivial image" width="100%" align="center"><br>por tamanho das palavras</div> <br>
      </td>
    </tr>
</tbody>
</table>



Outliers

<div align="center"><img src="imgs/outlier_length_tweets_posvsneg.png" alt="non-trivial image" width="50%" align="center"><br></div> <br>



Preprocesamento dos tweets.

- clean tweets
- tokenizer
- stop words
- punctuations word
- stemming



Palavras mais usadas nos diferentes tweets

<table>
<tbody>
  <tr>
      <td>
        <div align="center"><img src="imgs/freq_neg_words.png" alt="non-trivial image" width="80%" align="center"><br>racista/sexista</div> <br>
      </td>
  </tr>
  <tr>
      <td>
        <div align="center"><img src="imgs/freq_pos_words.png" alt="non-trivial image" width="80%" align="center"><br>não racista/sexista</div> <br>
      </td>
  </tr>
</tbody>
</table>



Assuntos que teriam uma probabilidade maior de gerar tweets (racista/sexista, não racista/sexista)

<table>
<tbody>
  <tr>
      <td>
        <div align="center"><img src="imgs/hashtag_neg.png" alt="non-trivial image" width="100%" align="center"><br>racista/sexista</div> <br>
      </td>
  </tr>
  <tr>
      <td>
        <div align="center"><img src="imgs/hashtag_pos.png" alt="non-trivial image" width="100%" align="center"><br>não racista/sexista</div> <br>
      </td>
  </tr>
</tbody>
</table>



#### Q2

O Time de mobile, gostaria de criar um app e utilizar o seu modelo. Qual arquitetura você sugere para utilizar o modelo em tempo real? Como resolveria a manutenção/atualização dos modelos?



- Trade off entre eficiência (menos recursos) e eficacia (melhor resultado)

  [**Recall**](https://www.iartificial.net/precision-recall-f1-accuracy-en-clasificacion/): De todos aqueles tweets que são ofensivos, quantos meu modelo consegue classificar como ofensivo.

  [**Precision**](https://www.iartificial.net/precision-recall-f1-accuracy-en-clasificacion/): De todos os tweets que meu modelo classificou como ofensivo, quantos realmente eram ofensivos.

<table>
<tbody>
  <tr>
      <td>
        <div align="center"><img src="imgs/acc.png" alt="non-trivial image" width="100%" align="center"><br>Accuracy</div> <br>
      </td>
      <td>
        <div align="center"><img src="imgs/prec.png" alt="non-trivial image" width="100%" align="center"><br>Precision</div> <br>
      </td>
    </tr>
  <tr>
      <td>
        <div align="center"><img src="imgs/rec.png" alt="non-trivial image" width="100%" align="center"><br>Recall</div> <br>
      </td>
      <td>
        <div align="center"><img src="imgs/f1.png" alt="non-trivial image" width="100%" align="center"><br>F-measure</div> <br>
      </td>
    </tr>
</tbody>
</table>



Tempo de execução

<div align="center">
<table>
<thead>
  <tr>
    <th>Model</th>
    <th>Time</th>
    <th>Model</th>
    <th>Time</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>NB</td>
    <td rowspan="6">~0.01ms - 0.1ms</td>
    <td>LSTM</td>
    <td rowspan="4">~3ms - 4ms</td>
  </tr>
  <tr>
    <td>RF</td>
    <td>MLP</td>
  </tr>
  <tr>
    <td>SVM</td>
    <td>BERT</td>
  </tr>
  <tr>
    <td>LR</td>
    <td>BiLSTM</td>
  </tr>
  <tr>
    <td>KNN</td>
    <td colspan="2" rowspan="2"></td>
  </tr>
  <tr>
    <td>GBM</td>
  </tr>
</tbody>
</table>
</div>



Manutenção/atualização

Fine-tunning

Frezze



#### Q3

De quais outras formas, você recomendaria que o seu modelo fosse utilizado?

para escalar

- parallel programming

- multi-gpus

- system distribution

- cloud

dataset imbalanced

- loss weighted
- sampling -> overffiting
- GTP3 -> generation